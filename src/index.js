var HttpClient = require('./lib/http-client');
var events = require('events');
var inherits = require('inherits');
var StreamAPI = require('./lib/stream_api');
var _ = require('lodash');
var Logger = require('./lib/logger');

function Klic(options){
    options         = options || {};
    this.base_url   = options.url || "https://api.2klic.io";
    this.apiVersion = options.apiVersion || "1.0";
    this.auth       = options.auth || {};

    this.logger     = new Logger((options.log_level >= 0) ? options.log_level : 6);

    this.http = new HttpClient(this.base_url, {app_key: options.app_key, unsafe: options.unsafe, logger: this.logger, api_version: this.api_version, app_type: options.app_type, cert: options.cert, servername: options.servername });

    // Install all supported API endpoints
    this.alarm          = require("./lib/methods/alarm/alarm")({ platform: this });
    this.zones          = require("./lib/methods/alarm/zones")({ platform: this });
    //this.sectors        = require("./lib/methods/sectors")({ platform: this });
    this.noc            = require("./lib/methods/noc/noc")({ platform: this });
    this.cameras        = require("./lib/methods/cameras")({ platform: this });
    this.streams        = require("./lib/methods/streams")({ platform: this });
    this.devices        = require("./lib/methods/devices")({ platform: this });
    this.zwave          = require("./lib/methods/zwave")({ platform: this });
    this.events         = require("./lib/methods/events")({ platform: this });
    this.locations      = require("./lib/methods/locations")({ platform: this });
    this.scenarios      = require("./lib/methods/scenarios")({ platform: this });
    this.schedules      = require("./lib/methods/schedules")({ platform: this });
    this.models         = require("./lib/methods/models")({ platform: this });
    this.notifications  = require("./lib/methods/notifications")({ platform: this });
    this.permissions    = require("./lib/methods/permissions")({ platform: this });
    this.translations   = require("./lib/methods/translations")({ platform: this });
    this.user           = require("./lib/methods/user")({ platform: this });
    this.templates      = require("./lib/methods/templates")({ platform: this });
    this.system         = require("./lib/methods/system")({ platform: this });
    this.access         = require("./lib/methods/access/access")({ platform: this });
    this.authentication = require("./lib/methods/auth")({ platform: this });
    this.groups         = require("./lib/methods/groups")({ platform: this });
}

inherits(Klic, events.EventEmitter);

Klic.prototype.authenticate = require('./lib/methods/authenticate');
Klic.prototype.register = require('./lib/methods/register');

module.exports = Klic;
