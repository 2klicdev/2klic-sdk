'use strict';

function Streams(options) {
    options = options || {};
    this.platform = options.platform;
}

Streams.prototype.list = function() {
    return this.platform.http.get('/streams', { token: this.platform.auth.token });
};

Streams.prototype.listRecord = function( device ) {
    return this.platform.http.get('/streams/record/' + device, { token: this.platform.auth.token });
};

Streams.prototype.startRecord = function( device, quality ) {
    return this.platform.http.get('/streams/record/' + device + '/' + quality + '/start', { token: this.platform.auth.token });
};

Streams.prototype.stopRecord = function( device, quality ) {
    return this.platform.http.get('/streams/record/' + device + '/' + quality + '/stop', { token: this.platform.auth.token });
};

Streams.prototype.startPlayback = function( record, timestamp ) {
    return this.platform.http.get('/streams/playback/' + record + '/start/' + timestamp, { token: this.platform.auth.token });
};

Streams.prototype.updatePlayback = function( record, timestamp ) {
    return this.platform.http.get('/streams/playback/' + record + '/update/' + timestamp, { token: this.platform.auth.token });
};

Streams.prototype.stopPlayback = function( record ) {
    return this.platform.http.get('/streams/playback/' + record + '/stop', { token: this.platform.auth.token });
};

module.exports = function(options) {
    options = options || {};
    return new Streams(options);
};

