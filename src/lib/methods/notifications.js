'use strict';

function Notifications(options) {
    options = options || {};
    this.platform = options.platform;
}

Notifications.prototype.list = function(params){
    return this.platform.http.get('/notifications', { query: params, token: this.platform.auth.token });
};

Notifications.prototype.get = function(locationId, params){
    return this.platform.http.get('/locations/'+locationId+'/notifications', { query: params, token: this.platform.auth.token });
};

Notifications.prototype.patch = function(notificationId, obj){
    return this.platform.http.patch('/notifications/'+notificationId, obj, { token: this.platform.auth.token });
};

Notifications.prototype.markRead = function(notificationId){
    return this.platform.http.patch('/notifications/'+notificationId, { read: true, readTs: Date.now() }, { token: this.platform.auth.token });
};

Notifications.prototype.getConfig = function(){
    return this.platform.http.get('/notifications/config', { token: this.platform.auth.token });
};

Notifications.prototype.updateConfig = function(config){
    return this.platform.http.put('/notifications/config' + (config._id ? "/"+config._id:""), config, { token: this.platform.auth.token });
};

module.exports = function(options) {
    options = options || {};
    return new Notifications(options);
};

