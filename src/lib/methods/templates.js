'use strict';

function Templates(options) {
    options = options || {};
    this.platform = options.platform;
}

Templates.prototype.list = function(){
    return this.platform.http.get('/templates', { token: this.platform.auth.token });
};

Templates.prototype.get = function(templateId, level){
    return this.platform.http.get('/templates/'+templateId, { query: { level: level }, token: this.platform.auth.token });
};

Templates.prototype.create = function(template){
    return this.platform.http.post('/templates', template, { token: this.platform.auth.token });
};

Templates.prototype.patch = function(obj, templateId){
    return this.platform.http.patch('/templates/'+templateId, obj, { token: this.platform.auth.token });
};

Templates.prototype.delete = function(templateId){
    return this.platform.http.delete('/templates/'+templateId, { token: this.platform.auth.token });
};

module.exports = function(options) {
    options = options || {};
    return new Templates(options);
};
