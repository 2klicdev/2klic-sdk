'use strict';

function Groups(options) {
    options = options || {};
    this.platform = options.platform;
}

Groups.prototype.list = function(query){
    return this.platform.http.get('/groups', { token: this.platform.auth.token });
};

Groups.prototype.create = function(group){
    return this.platform.http.post('/groups', group, { token: this.platform.auth.token });
};

Groups.prototype.get = function(groupId){
    return this.platform.http.get('/groups/' + groupId, { token: this.platform.auth.token });
};

Groups.prototype.update = function(groupId, group){
    return this.platform.http.get('/groups/' + groupId, group, { token: this.platform.auth.token });
};

Groups.prototype.delete = function(groupId){
    return this.platform.http.delete('/groups/' + groupId, { token: this.platform.auth.token });
};

module.exports = function(options) {
    options = options || {};
    return new Groups(options);
};
