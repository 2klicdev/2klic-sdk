module.exports = (function() {

    return function(user, options) {
        var _this = this, logger = this.logger;
        options = options || {};

        logger.debug("Executing HTTP request GET /auth/register");
        return this.http.post("/auth/register", user, options).then(function(resp) {
            logger.debug(resp);
            if(resp) {
                return resp.data;
            }
            else {
                logger.error("Invalid response received from platform");
            }

        });

    };

})();
