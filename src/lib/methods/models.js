'use strict';

function Models(options) {
    options = options || {};
    this.platform = options.platform;
}

Models.prototype.list = function(query){
    return this.platform.http.get('/models', { query: query, token: this.platform.auth.token });
};

Models.prototype.get = function(id, query){
    return this.platform.http.get('/models/'+id,  { query: query, token: this.platform.auth.token });
};

Models.prototype.search = function(query){
    return this.platform.http.get('/models',  { query: query, token: this.platform.auth.token });
};

module.exports = function(options) {
    options = options || {};
    return new Models(options);
};

