'use strict';

function Sectors(options) {
    options = options || {};
    this.platform = options.platform;
}

// Sectors
Sectors.prototype.create = function(alarmId, sector){
    return this.platform.http.post('/alarm/'+alarmId+'/sectors', sector, { token: this.platform.auth.token });
};

Sectors.prototype.list = function(alarmId){
    return this.platform.http.get('/alarm/'+alarmId+'/sectors', { token: this.platform.auth.token });
};

Sectors.prototype.get = function(alarmId, sectorId){
    return this.platform.http.get('/alarm/'+alarmId+'/sectors/'+sectorId, { token: this.platform.auth.token });
};

Sectors.prototype.update = function(alarmId, sectorId, sector){
    return this.platform.http.put('/alarm/'+alarmId+'/sectors/'+sectorId, sector, { token: this.platform.auth.token });
};

Sectors.prototype.delete = function(alarmId, sectorId){
    return this.platform.http.delete('/alarm/'+alarmId+'/sectors/'+sectorId, { token: this.platform.auth.token });
};

module.exports = function(options) {
    options = options || {};
    return new Sectors(options);
};

