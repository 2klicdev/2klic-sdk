'use strict';
var Access = require("./access");
var Zone = require("./zones");

function Alarm(options) {
    options = options || {};
    this.platform = options.platform;
    this.access = new Access({ platform: this.platform });
    this.zones = new Zone({ platform: this.platform });
}

// Provision an alarm system
Alarm.prototype.create = function(alarm){
    return this.platform.http.post('/alarms', alarm, { token: this.platform.auth.token });
};

// Get a an alarm system status
Alarm.prototype.get = function(alarmId){
    return this.platform.http.get('/alarms/'+alarmId, { token: this.platform.auth.token });
};

Alarm.prototype.list = function(userId){
    return this.platform.http.get('/alarms' + (userId ? '?user=' + userId : ''), { token: this.platform.auth.token });
};

Alarm.prototype.patch = function(alarm, alarmId){
    return this.platform.http.patch('/alarms/'+alarmId, alarm, { token: this.platform.auth.token });
};

Alarm.prototype.delete = function(alarmId){
    return this.platform.http.delete('/alarms/'+alarmId, { token: this.platform.auth.token });
};

Alarm.prototype.getToken = function(alarmId, pin){
    return this.platform.http.post('/alarms/'+alarmId+'/auth/token', { pin: pin }, { token: this.platform.auth.token });
};

Alarm.prototype.arm = function(alarmId, armToken, status, bypassedZoneIds){
    return this.platform.http.put('/alarms/'+alarmId, { status: status, bypassed: bypassedZoneIds }, { query: { armToken: armToken }, token: this.platform.auth.token });
};

// Suspend 24/7 zones that can't be armed because they are in alarm
Alarm.prototype.suspend = function(alarmId, armToken, zones, timeout, rearm){
    return this.platform.http.put('/alarms/'+alarmId, { status: 'suspend', zones: zones, timeout: timeout, rearm: rearm }, { query: { armToken: armToken }, token: this.platform.auth.token });
};


module.exports = function(options) {
    options = options || {};
    return new Alarm(options);
};

