'use strict';

function Access(options) {
    options = options || {};
    this.platform = options.platform;
}

Access.prototype.list = function(alarmId){
    return this.platform.http.get('/alarms/'+alarmId+'/access', { token: this.platform.auth.token });
};

Access.prototype.create = function(alarmId, access){
    return this.platform.http.post('/alarms/'+alarmId+'/access', access, { token: this.platform.auth.token });
};

Access.prototype.update = function(alarmId, access, accessId){
    return this.platform.http.put('/alarms/'+alarmId+'/access/'+accessId, access, { token: this.platform.auth.token });
};

Access.prototype.patch = function(alarmId, access, accessId){
    return this.platform.http.patch('/alarms/'+alarmId+'/access/'+accessId, access, { token: this.platform.auth.token });
};

Access.prototype.delete = function(alarmId, accessId){
    return this.platform.http.delete('/alarms/'+alarmId+'/access/'+accessId, { token: this.platform.auth.token });
};

module.exports = function(options) {
    options = options || {};
    return new Access(options);
};

