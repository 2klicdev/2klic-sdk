'use strict';

var _ = require('lodash');

function Zones(options) {
    options = options || {};
    this.platform = options.platform;
}

// Zones
Zones.prototype.create = function(alarmId, zone, query){
    return this.platform.http.post('/alarms/'+alarmId+'/zones', zone, { query: query, token: this.platform.auth.token });
};

Zones.prototype.list = function(alarmId, query){
    return this.platform.http.get('/alarms/'+alarmId+'/zones', { query: query, token: this.platform.auth.token });
};

Zones.prototype.get = function(alarmId, zoneId, query){
    return this.platform.http.get('/alarms/'+alarmId+'/zones/'+zoneId, { query: query, token: this.platform.auth.token });
};

Zones.prototype.update = function(alarmId, zone, query){
    return this.platform.http.put('/alarms/'+alarmId+'/zones/'+zone._id, zone, { query: query, token: this.platform.auth.token });
};

Zones.prototype.patch = function(alarmId, zone, query){
    return this.platform.http.patch('/alarms/'+alarmId+'/zones/'+zone._id, zone, { query: query, token: this.platform.auth.token });
};

Zones.prototype.delete = function(alarmId, zoneId, query){
    if(zoneId) return this.platform.http.delete('/alarms/'+alarmId+'/zones/'+zoneId, { token: this.platform.auth.token });
    else return this.platform.http.delete('/alarms/'+alarmId+'/zones', { query: query, token: this.platform.auth.token });
};

module.exports = function(options) {
    options = options || {};
    return new Zones(options);
};

