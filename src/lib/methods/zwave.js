'use strict';

function ZWave(options) {
    options = options || {};
    this.platform = options.platform;
}

ZWave.prototype.exclusion = function(gatewayId){
    return this.platform.http.post('/devices/'+gatewayId+'/zwave/exclusion', {}, { token: this.platform.auth.token });
};

ZWave.prototype.removeFailed = function(gatewayId){
    return this.platform.http.delete('/devices/'+gatewayId+'/zwave/failed', {}, { token: this.platform.auth.token });
};

ZWave.prototype.info = function(deviceId){
    return this.platform.http.get('/devices/'+deviceId+'/zwave', { token: this.platform.auth.token });
};

ZWave.prototype.cancel = function(gatewayId){
    return this.platform.http.post('/devices/'+gatewayId+'/zwave/cancel', {}, { token: this.platform.auth.token });
};

// ------------ S2 Zwave method ---------------

ZWave.prototype.interview = function(gatewayId){
    return this.platform.http.post('/devices/'+gatewayId+'/zwave/interview', {}, { token: this.platform.auth.token });
};

ZWave.prototype.getDsk = function(gatewayId){
    return this.platform.http.get('/devices/'+gatewayId+'/zwave/dsk', { token: this.platform.auth.token });
};

ZWave.prototype.getKeyRequest = function(gatewayId){
    return this.platform.http.get('/devices/'+gatewayId+'/zwave/keyRequest', { token: this.platform.auth.token });
};

ZWave.prototype.postDsk = function(gatewayId, payload){
    return this.platform.http.post('/devices/'+gatewayId+'/zwave/dsk', payload, { token: this.platform.auth.token });
};

ZWave.prototype.postKeyRequest = function(gatewayId, payload){
    return this.platform.http.post('/devices/'+gatewayId+'/zwave/keyRequest', payload, { token: this.platform.auth.token });
};
ZWave.prototype.learn = function(gatewayId, payload){
    return this.platform.http.post('/devices/'+gatewayId+'/zwave/learn', payload, { token: this.platform.auth.token });
};
ZWave.prototype.replace = function(deviceId){
    return this.platform.http.post('/devices/'+deviceId+'/zwave/replace', {}, { token: this.platform.auth.token });
};
ZWave.prototype.update = function(gatewayId){
    return this.platform.http.post('/devices/'+gatewayId+'/zwave/update', {}, { token: this.platform.auth.token });
};
ZWave.prototype.reset = function(gatewayId){
    return this.platform.http.post('/devices/'+gatewayId+'/zwave/reset', {}, { token: this.platform.auth.token });
};
ZWave.prototype.sendNif = function(gatewayId){
    return this.platform.http.post('/devices/'+gatewayId+'/zwave/nif', {}, { token: this.platform.auth.token });
};

module.exports = function(options) {
    options = options || {};
    return new ZWave(options);
};

