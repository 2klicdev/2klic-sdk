'use strict';

function User(options) {
    options = options || {};
    this.platform = options.platform;
}

User.prototype.get = function(username){
    return this.platform.http.get('/users/'+username, { token: this.platform.auth.token });
};

User.prototype.register = function(user){
    return this.platform.http.post('/auth/register', user);
};

User.prototype.forgot = function(email){
    return this.platform.http.get('/auth/forgot/'+email);
};

User.prototype.resendValidateEmail = function(email){
    return this.platform.http.get('/auth/resend/'+email);
};

User.prototype.reset = function(resetToken){
    return this.platform.http.get('/auth/reset/'+resetToken);
};

User.prototype.resetPassword = function(resetToken, passwordInfo){
    return this.platform.http.post('/auth/reset/'+resetToken, passwordInfo);
};

User.prototype.confirm = function(key){
    return this.platform.http.get('/auth/confirm/'+key);
};

User.prototype.changePassword = function(username, currentPassword, password){
    return this.platform.http.put('/users/'+username+'/password', { current:currentPassword, password:password }, { token: this.platform.auth.token });
};

User.prototype.list = function(){
    return this.platform.http.get('/users', { token: this.platform.auth.token });
};

User.prototype.listAlarmSystems = function(userId){
    return this.platform.http.get('/users/' + userId + '/alarms', { token: this.platform.auth.token });
};

User.prototype.getPermissions = function(userId, params){
    return this.platform.http.get('/users/'+userId+'/permissions', { query: params, token: this.platform.auth.token });
};

User.prototype.getCurrentUser = function(options){
    var token;
    options ? token = options.token : token = undefined;
    return this.platform.http.get('/users/me', { token: token || this.platform.auth.token });
};

User.prototype.update = function(username, user){
    return this.platform.http.put('/users/'+username, user, { token: this.platform.auth.token });
};

User.prototype.patch = function(username, obj){
    return this.platform.http.patch('/users/' + username, obj, { token: this.platform.auth.token });
};

User.prototype.updateProfile = function(username, type, obj){
    return this.platform.http.patch('/users/' + username + '/profiles/' + type, obj, { token: this.platform.auth.token });
};

User.prototype.enable = function(username){
    return this.platform.http.patch('/users/' + username + '/enable', {}, { token: this.platform.auth.token });
};

User.prototype.delete = function(username){
    return this.platform.http.delete('/users/'+username, { token: this.platform.auth.token });
};

User.prototype.bindDevice = function(code, location, name){
    var payload = { code: code, location: location };
    if (name) payload.name = name;
    return this.platform.http.post('/devices/bind', payload, { token: this.platform.auth.token });
};

module.exports = function(options) {
    options = options || {};
    return new User(options);
};
