'use strict';

function Events(options) {
    options = options || {};
    this.platform = options.platform;
}

Events.prototype.create = function(resourcePath, eventType, resource, timestamp, requestId){
    var event = resourcePath;
    if (typeof event === 'string') {
        event = { path: resourcePath, resource: resource, timestamp: timestamp, type: eventType, request: requestId };
    }
    return this.platform.http.post('/events', event, { token: this.platform.auth.token });
};

module.exports = function(options) {
    options = options || {};
    return new Events(options);
};

