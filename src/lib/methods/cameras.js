'use strict';

function Cameras(options) {
    options = options || {};
    this.platform = options.platform;
}

Cameras.prototype.get = function(propertyId){
    return this.platform.http.get('/homes/'+propertyId+'/cameras/stream/all', { token: this.platform.auth.token });
};

module.exports = function(options) {
    options = options || {};
    return new Cameras(options);
};

