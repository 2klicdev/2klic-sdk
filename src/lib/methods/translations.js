'use strict';

function Translations(options) {
    options = options || {};
    this.platform = options.platform;
}

Translations.prototype.list = function(){
    return this.platform.http.get('/translations', { token: this.platform.auth.token });
};

Translations.prototype.get = function(key){
    return this.platform.http.get('/translations/'+key, { token: this.platform.auth.token });
};

Translations.prototype.create = function(translation){
    return this.platform.http.post('/translations', translation, { token: this.platform.auth.token });
};

Translations.prototype.update = function(key, translation){
    return this.platform.http.put('/translations/'+key, translation, { token: this.platform.auth.token });
};


module.exports = function(options) {
    options = options || {};
    return new Translations(options);
};

