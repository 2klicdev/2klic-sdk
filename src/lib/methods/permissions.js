'use strict';

function Permissions(options) {
    options = options || {};
    this.platform = options.platform;
}

Permissions.prototype.list = function(userId){
    return this.platform.http.get('/permissions', { token: this.platform.auth.token, whoType: 'user', who: userId });
};

module.exports = function(options) {
    options = options || {};
    return new Permissions(options);
};
