'use strict';

var _ = require('lodash');

function Devices(options) {
    options = options || {};
    this.platform = options.platform;
}

Devices.prototype.list = function(query){
    return this.platform.http.get('/devices', { query: query, token: this.platform.auth.token });
};

Devices.prototype.get = function(deviceId, query){
    return this.platform.http.get('/devices/'+deviceId, { query: query, token: this.platform.auth.token });
};

Devices.prototype.getChildren = function(deviceId, level, query){
    var query = _.merge({ showChildren: true, level: level }, query);
    return this.platform.http.get('/devices/'+deviceId, { query: query, token: this.platform.auth.token });
};

Devices.prototype.create = function(device, query){
    return this.platform.http.post('/devices', device, { query: query, token: this.platform.auth.token });
};

Devices.prototype.update = function(device, query){
    return this.platform.http.put('/devices/'+device._id, device, { query: query, token: this.platform.auth.token });
};

Devices.prototype.patch = function(obj, query){
    return this.platform.http.patch('/devices/'+obj._id, obj, { query: query, token: this.platform.auth.token });
};

Devices.prototype.delete = function(deviceId, query){
    return this.platform.http.delete('/devices/'+deviceId, { query: query, token: this.platform.auth.token });
};

// Reset a hub
Devices.prototype.reset = function(deviceId){
    return this.platform.http.post('/devices/'+deviceId+'/reset', {}, { token: this.platform.auth.token });
};

Devices.prototype.updateCapability = function(deviceId, capabilityId, value, unit, timestamp){
    return this.platform.http.put('/devices/'+deviceId+'/caps/'+capabilityId, { value: value, unit: unit, timestamp: timestamp }, { token: this.platform.auth.token });
};

Devices.prototype.patchCapability = function(deviceId, capabilityId, obj){
    return this.platform.http.patch('/devices/'+deviceId+'/caps/'+capabilityId, obj, { token: this.platform.auth.token });
};

Devices.prototype.getCapability = function(deviceId, capabilityId){
    return this.platform.http.get('/devices/'+deviceId+'/caps/'+capabilityId, { token: this.platform.auth.token });
};

Devices.prototype.listCapability = function(deviceId){
    if(deviceId) return this.platform.http.get('/devices/'+deviceId+'/caps', { token: this.platform.auth.token });
    else return this.platform.http.get('/caps', { token: this.platform.auth.token });
};

// Hub provisioning
Devices.prototype.provision = function(name, mac, model, location){
    var _this = this;
    var obj = _.merge({ name:name, mac:mac, model:model }, { location: location }); // optional location parameter
    return this.platform.http.post('/devices', obj)
        .then(function(res){
            //logger.debug("Installing token %s as default platform auth mechanism", resp.data.token);
            if(res.data.token) _this.platform.auth.token = res.data.token;
            return res;
        })
};

// Controller Heartbeat
Devices.prototype.heartbeat = function(deviceId, sysHealth){
    return this.platform.http.post('/devices/'+deviceId+'/heartbeat', sysHealth, { token: this.platform.auth.token });
};

Devices.prototype.listHeartbeat = function(deviceId, query){
    return this.platform.http.get('/devices/'+deviceId+'/heartbeat', { query: query, token: this.platform.auth.token });
};

// Controller ping
Devices.prototype.ping = function(deviceId){
    return this.platform.http.post('/devices/'+deviceId+'/ping', {}, { token: this.platform.auth.token });
};

// External ping of controller
Devices.prototype.pingGateway = function(gatewayId){
    return this.platform.http.get('/devices/'+gatewayId+'/ping', { token: this.platform.auth.token });
};


Devices.prototype.unbind = function(gateway, password){
    return this.platform.http.put('/devices/unbind', { gateway: gateway, password: password }, { token: this.platform.auth.token });
};

module.exports = function(options) {
    options = options || {};
    return new Devices(options);
};

