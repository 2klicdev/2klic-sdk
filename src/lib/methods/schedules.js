'use strict';

function Schedules(options) {
    options = options || {};
    this.platform = options.platform;
}

Schedules.prototype.list = function(){
    return this.platform.http.get('/schedules', { token: this.platform.auth.token });
};

Schedules.prototype.get = function(scheduleId){
    return this.platform.http.get('/schedules/'+scheduleId, { token: this.platform.auth.token });
};

Schedules.prototype.create = function(schedule){
    return this.platform.http.post('/schedules', schedule, { token: this.platform.auth.token });
};

Schedules.prototype.update = function(schedule){
    return this.platform.http.put('/schedules/'+schedule._id, schedule, { token: this.platform.auth.token });
};

Schedules.prototype.delete = function(scheduleId){
    return this.platform.http.delete('/schedules/'+scheduleId, { token: this.platform.auth.token });
};

module.exports = function(options) {
    options = options || {};
    return new Schedules(options);
};

