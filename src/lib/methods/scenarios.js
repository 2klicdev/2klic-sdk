'use strict';

function Scenarios(options) {
    options = options || {};
    this.platform = options.platform;
}

Scenarios.prototype.list = function(){
    return this.platform.http.get('/scenarios', { token: this.platform.auth.token });
};

Scenarios.prototype.get = function(scenarioId){
    return this.platform.http.get('/scenarios/'+scenarioId, { token: this.platform.auth.token });
};

Scenarios.prototype.create = function(scenario){
    return this.platform.http.post('/scenarios', scenario, { token: this.platform.auth.token });
};

Scenarios.prototype.update = function(scenario){
    return this.platform.http.put('/scenarios/'+scenario._id, scenario, { token: this.platform.auth.token });
};

Scenarios.prototype.delete = function(scenarioId){
    return this.platform.http.delete('/scenarios/'+scenarioId, { token: this.platform.auth.token });
};

module.exports = function(options) {
    options = options || {};
    return new Scenarios(options);
};

