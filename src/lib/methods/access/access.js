'use strict';
var Cards = require("./cards");

function Access(options) {
    options = options || {};
    this.platform = options.platform;
    this.cards = new Cards ({ platform: this.platform });
}

Access.prototype.list = function(params){
    return this.platform.http.get('/access/', { query: params, token: this.platform.auth.token });
};

Access.prototype.get = function(id, params){
    return this.platform.http.get('/access/'+id, { query: params, token: this.platform.auth.token });
};

Access.prototype.create = function(accessData){
    return this.platform.http.post('/access', accessData, { token: this.platform.auth.token });
};

Access.prototype.update = function(id, accessData){
    return this.platform.http.put('/access/' + id, accessData, { token: this.platform.auth.token });
};

Access.prototype.patch = function(id, accessData){
    return this.platform.http.patch('/access/' + d, accessData, { token: this.platform.auth.token });
};

Access.prototype.patch = function(id, accessData){
    return this.platform.http.patch('/access/' + id, accessData, { token: this.platform.auth.token });
};


module.exports = function(options) {
    options = options || {};
    return new Access(options);
};
