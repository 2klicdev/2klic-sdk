'use strict';

function Cards(options) {
    options = options || {};
    this.platform = options.platform;
}

Cards.prototype.list= function(){
    return this.platform.http.get('/access/cards', { token: this.platform.auth.token });
};

Cards.prototype.get= function(id){
    return this.platform.http.get('/access/cards/' + id, { token: this.platform.auth.token });
};

Cards.prototype.create= function(cardData){
    return this.platform.http.post('/access/cards', cardData, { token: this.platform.auth.token });
};

Cards.prototype.update= function(id, cardData){
    return this.platform.http.put('/access/cards/' + id, cardData, { token: this.platform.auth.token });
};

Cards.prototype.patch = function(id, cardData){
    return this.platform.http.patch ('/access/cards/' + id, cardData, { token: this.platform.auth.token });
};

module.exports = function(options) {
    options = options || {};
    return new Cards(options);
};
