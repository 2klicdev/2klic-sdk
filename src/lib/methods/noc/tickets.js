'use strict';

function NocTickets(options) {
    options = options || {};
    this.platform = options.platform;
}

NocTickets.prototype.list = function(nocId){
    return this.platform.http.get('/nocs/' + nocId + '/noc_tickets', { token: this.platform.auth.token });
};

NocTickets.prototype.get = function(nocId, ticketId){
    return this.platform.http.get('/nocs/' + nocId + '/noc_tickets/' + ticketId, { token: this.platform.auth.token });
};

NocTickets.prototype.update = function(nocId, ticketId, ticket){
    return this.platform.http.put('/nocs/' + nocId + '/noc_tickets/' + ticketId, ticket, { token: this.platform.auth.token });
};

NocTickets.prototype.patch = function(nocId, ticketId, ticket){
    return this.platform.http.patch ('/nocs/' + nocId + '/noc_tickets/' + ticketId, ticket, { token: this.platform.auth.token });
};

NocTickets.prototype.create = function(nocId, ticket){
    return this.platform.http.post('/nocs' + nocId + '/noc_ticket', ticket, { token: this.platform.auth.token });
};

NocTickets.prototype.listDevices = function(nocId, ticketId){
    return this.platform.http.get('/nocs/' + nocId + '/noc_tickets/' + ticketId + '/devices', { token: this.platform.auth.token });
};

NocTickets.prototype.listEvents = function(nocId, ticketId){
    return this.platform.http.get('/nocs/' + nocId + '/noc_tickets/' + ticketId + '/noc_ticket_events', { token: this.platform.auth.token });
};

NocTickets.prototype.getEvent = function(nocId, ticketId, ticketEventId){
    return this.platform.http.get('/nocs/' + nocId + '/noc_tickets/' + ticketId + '/noc_ticket_events/' + ticketEventId, { token: this.platform.auth.token });
};

NocTickets.prototype.createEvent = function(nocId, ticketId, ticketEvent){
    return this.platform.http.post('/nocs/' + nocId + '/noc_tickets/' + ticketId + '/noc_ticket_events', ticketEvent, { token: this.platform.auth.token });
};

NocTickets.prototype.updateEvent= function(nocId, ticketId, ticketEventId, ticketEvent){
    return this.platform.http.put('/nocs/' + nocId + '/noc_tickets/' + ticketId + '/noc_ticket_events/' + ticketEventId, ticketEvent, { token: this.platform.auth.token });
};

NocTickets.prototype.patchEvent = function(nocId, ticketId, ticketEventId, ticketEvent){
    return this.platform.http.patch ('/nocs/' + nocId + '/noc_tickets/' + ticketId + '/noc_ticket_events/' + ticketEventId, ticketEvent, { token: this.platform.auth.token });
};

module.exports = function(options) {
    options = options || {};
    return new NocTickets(options);
};
