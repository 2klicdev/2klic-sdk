'use strict';

function NocServices(options) {
    options = options || {};
    this.platform = options.platform;
}

NocServices.prototype.list= function(nocId){
    return this.platform.http.get('/nocs/' + nocId + '/services', { token: this.platform.auth.token });
};

NocServices.prototype.get= function(nocId, serviceId){
    return this.platform.http.get('/nocs/' + nocId + '/services/' + serviceId, { token: this.platform.auth.token });
};

NocServices.prototype.create= function(nocId, service){
    return this.platform.http.post('/nocs/' + nocId + '/services', service, { token: this.platform.auth.token });
};

NocServices.prototype.update= function(nocId, serviceId, service){
    return this.platform.http.put('/nocs/' + nocId + '/services/' + serviceId, service, { token: this.platform.auth.token });
};

NocServices.prototype.patch = function(nocId, serviceId, service){
    return this.platform.http.patch ('/nocs/' + nocId + '/services/' + serviceId, service, { token: this.platform.auth.token });
};

module.exports = function(options) {
    options = options || {};
    return new NocServices(options);
};
