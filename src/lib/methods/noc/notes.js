'use strict';

function NocNotes(options) {
    options = options || {};
    this.platform = options.platform;
}

NocNotes.prototype.list = function(nocId){
    return this.platform.http.get('/nocs/' + nocId + '/notes', { token: this.platform.auth.token });
};

NocNotes.prototype.get = function(nocId, noteId){
    return this.platform.http.get('/nocs/' + nocId + '/notes/' + noteId, { token: this.platform.auth.token });
};

NocNotes.prototype.update = function(nocId, noteId, note){
    return this.platform.http.put('/nocs/' + nocId + '/notes/' + noteId, note, { token: this.platform.auth.token });
};

NocNotes.prototype.updateByResource = function(nocId, resourceId, note){
    note.resource = resourceId;
    return this.platform.http.put('/nocs/' + nocId + '/notes', note, { token: this.platform.auth.token });
};

NocNotes.prototype.create = function(nocId, note){
    return this.platform.http.post('/nocs/' + nocId + '/notes', note, { token: this.platform.auth.token });
};

module.exports = function(options) {
    options = options || {};
    return new NocNotes(options);
};
