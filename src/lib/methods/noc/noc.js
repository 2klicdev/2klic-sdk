'use strict';
var NocCustomers = require("./customers");
var NocTickets = require("./tickets");
var NocServices = require("./services");
var NocServiceActions = require("./service_actions");
var NocNotes = require("./notes");

function Noc(options) {
    options = options || {};
    this.platform = options.platform;
    this.customers = new NocCustomers({ platform: this.platform });
    this.tickets = new NocTickets({ platform: this.platform });
    this.services = new NocServices({ platform: this.platform });
    this.service_actions = new NocServiceActions({ platform: this.platform });
    this.notes = new NocNotes({ platform: this.platform });
}

Noc.prototype.list = function() {
    return this.platform.http.get('/nocs', { token: this.platform.auth.token });
};

Noc.prototype.get = function(nocId){
    return this.platform.http.get('/nocs/' + nocId, { token: this.platform.auth.token });
};

Noc.prototype.create = function(noc){
    return this.platform.http.post('/nocs', noc, { token: this.platform.auth.token });
};

Noc.prototype.update = function(nocId, noc){
    return this.platform.http.put('/nocs/' + nocId, noc, { token: this.platform.auth.token });
};

Noc.prototype.patch = function(nocId, noc){
    return this.platform.http.patch('/nocs/' + nocId, noc, { token: this.platform.auth.token });
};

Noc.prototype.approve = function(nocId, approvalToken){
    return this.platform.http.put('/nocs/' + nocId + '/approve/' + approvalToken);
};

Noc.prototype.approval_link = function(email){
    return this.platform.http.get('/nocs/approval-link/' + email);
};

Noc.prototype.stats = function(filter){
    return this.platform.http.post('/nocs/stats', filter, { token: this.platform.auth.token });
};

module.exports = function(options) {
    options = options || {};
    return new Noc(options);
};
