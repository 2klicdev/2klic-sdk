'use strict';

function NocCustomers(options) {
    options = options || {};
    this.platform = options.platform;
}

NocCustomers.prototype.list= function(nocId){
    return this.platform.http.get('/nocs/' + nocId + '/customers', { token: this.platform.auth.token });
};

NocCustomers.prototype.get= function(nocId, customerId){
    return this.platform.http.get('/nocs/' + nocId + '/customers/' + customerId, { token: this.platform.auth.token });
};

NocCustomers.prototype.create= function(nocId, customer){
    return this.platform.http.post('/nocs/' + nocId + '/customers', customer, { token: this.platform.auth.token });
};

NocCustomers.prototype.update= function(nocId, customerId, customer){
    return this.platform.http.put('/nocs/' + nocId + '/customers/' + customerId, customer, { token: this.platform.auth.token });
};

NocCustomers.prototype.patch = function(nocId, customerId, customer){
    return this.platform.http.patch ('/nocs/' + nocId + '/customers/' + customerId, customer, { token: this.platform.auth.token });
};

NocCustomers.prototype.listDevices = function(nocId, customerId, alarmSystemId){
    return this.platform.http.get('/nocs/' + nocId + '/customers/' + customerId + '/devices/' + alarmSystemId, { token: this.platform.auth.token });
};

NocCustomers.prototype.getPredefinedServiceActions= function(nocId, customerId, alarmSystemId){
    return this.platform.http.get('/nocs/' + nocId + '/customers/' + customerId + '/alarmsystem/' + alarmSystemId + '/predefined-service-actions', { token: this.platform.auth.token });
};

NocCustomers.prototype.listServiceActions= function(nocId, customerId){
    return this.platform.http.get('/nocs/' + nocId + '/customers/' + customerId + '/actions', { token: this.platform.auth.token });
};

NocCustomers.prototype.getServiceAction= function(nocId, customerId, actionId){
    return this.platform.http.get('/nocs/' + nocId + '/customers/' + customerId + '/actions/' + actionId, { token: this.platform.auth.token });
};

NocCustomers.prototype.createServiceAction= function(nocId, customerId, actionData){
    return this.platform.http.post('/nocs/' + nocId + '/customers/' + customerId + '/actions', actionData, { token: this.platform.auth.token });
};

NocCustomers.prototype.updateServiceAction = function(nocId, customerId, actionId, actionData){
    return this.platform.http.put('/nocs/' + nocId + '/customers/' + customerId + '/actions/' + actionId, actionData, { token: this.platform.auth.token });
};

NocCustomers.prototype.listInstallations= function(nocId){
    return this.platform.http.get('/nocs/' + nocId + '/customers/installations', { token: this.platform.auth.token });
};

NocCustomers.prototype.getInstallation= function(nocId, installationId){
    return this.platform.http.get('/nocs/' + nocId + '/customers/installations/' + installationId, { token: this.platform.auth.token });
};

NocCustomers.prototype.createInstallation= function(nocId, installationData){
    return this.platform.http.post('/nocs/' + nocId + '/customers/installations', installationData, { token: this.platform.auth.token });
};

NocCustomers.prototype.updateInstallation= function(nocId, installationId, installationData){
    return this.platform.http.put('/nocs/' + nocId + '/customers/installations/' + installationId, installationData, { token: this.platform.auth.token });
};

NocCustomers.prototype.searchInstallers = function(nocId, searchCriteria){
    return this.platform.http.post('/nocs/' + nocId + '/customers/installations/searchinstallers', searchCriteria, { token: this.platform.auth.token });
};

NocCustomers.prototype.updateNocModelType = function(nocId, customerId, deviceId, nocModelType){
    return this.platform.http.patch('/nocs/' + nocId + '/customers/' + customerId + '/devices/' + deviceId, nocModelType, { token: this.platform.auth.token });
};

NocCustomers.prototype.addUser = function(nocId, customerId, userData){
    return this.platform.http.post('/nocs/' + nocId + '/customers/' + customerId + '/userlist', userData, { token: this.platform.auth.token });
};

NocCustomers.prototype.deleteUser = function(nocId, customerId, userId){
    return this.platform.http.delete('/nocs/' + nocId + '/customers/' + customerId + '/userlist/' + userId, { token: this.platform.auth.token });
};

NocCustomers.prototype.updateUser = function(nocId, customerId, userId, userData){
    return this.platform.http.patch('/nocs/' + nocId + '/customers/' + customerId + '/userlist/' + userId, userData, { token: this.platform.auth.token });
};


NocCustomers.prototype.addUserCall = function(nocId, customerId, alarmSystemId, userData){
    return this.platform.http.post('/nocs/' + nocId + '/customers/' + customerId + '/alarmsystem/' + alarmSystemId + '/calllist', userData, { token: this.platform.auth.token });
};

NocCustomers.prototype.deleteUserCall = function(nocId, customerId, alarmSystemId, userId){
    return this.platform.http.delete('/nocs/' + nocId + '/customers/' + customerId + '/alarmsystem/' + alarmSystemId + '/calllist/' + userId, { token: this.platform.auth.token });
};

NocCustomers.prototype.updateUserCall = function(nocId, customerId, alarmSystemId, userId, userData){
    return this.platform.http.patch('/nocs/' + nocId + '/customers/' + customerId + '/alarmsystem/' + alarmSystemId + '/calllist/' + userId, userData, { token: this.platform.auth.token });
};

module.exports = function(options) {
    options = options || {};
    return new NocCustomers(options);
};
