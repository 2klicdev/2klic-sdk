'use strict';

function NocServiceActions(options) {
    options = options || {};
    this.platform = options.platform;
}

NocServiceActions.prototype.list= function(nocId){
    return this.platform.http.get('/nocs/' + nocId + '/service_actions', { token: this.platform.auth.token });
};

NocServiceActions.prototype.get= function(nocId, serviceId){
    return this.platform.http.get('/nocs/' + nocId + '/service_actions/' + serviceId, { token: this.platform.auth.token });
};

NocServiceActions.prototype.getPredefinedServiceActions= function(nocId){
    return this.platform.http.get('/nocs/' + nocId + '/service_actions/predefined', { token: this.platform.auth.token });
};

NocServiceActions.prototype.create= function(nocId, serviceActions){
    return this.platform.http.post('/nocs/' + nocId + '/service_actions', serviceActions, { token: this.platform.auth.token });
};

NocServiceActions.prototype.update= function(nocId, serviceId, serviceActions){
    return this.platform.http.put('/nocs/' + nocId + '/service_actions/' + serviceId, serviceActions, { token: this.platform.auth.token });
};

NocServiceActions.prototype.patch = function(nocId, serviceId, serviceActions){
    return this.platform.http.patch ('/nocs/' + nocId + '/service_actions/' + serviceId, serviceActions, { token: this.platform.auth.token });
};

module.exports = function(options) {
    options = options || {};
    return new NocServiceActions(options);
};
