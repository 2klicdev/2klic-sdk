'use strict';

function Locations(options) {
    options = options || {};
    this.platform = options.platform;
}

Locations.prototype.list = function(level){
    return this.platform.http.get('/locations', { query:{ level: level }, token: this.platform.auth.token });
};

Locations.prototype.get = function(locationId, level){
    return this.platform.http.get('/locations/'+locationId, { query: { level: level }, token: this.platform.auth.token });
};

Locations.prototype.getRoot = function(locationId, level){
    return this.platform.http.get('/locations/'+locationId, { query: { root: true, level: level }, token: this.platform.auth.token });
};

Locations.prototype.create = function(location){
    return this.platform.http.post('/locations', location, { token: this.platform.auth.token });
};

Locations.prototype.update = function(location){
    return this.platform.http.put('/locations/'+location._id, location, { token: this.platform.auth.token });
};

Locations.prototype.patch = function(obj, locationId){
    return this.platform.http.patch('/locations/'+locationId, obj, { token: this.platform.auth.token });
};

Locations.prototype.delete = function(locationId){
    return this.platform.http.delete('/locations/'+locationId, { token: this.platform.auth.token });
};

Locations.prototype.getOwners = function(locationId){
    return this.platform.http.get('/locations/'+locationId+'/owners', { token: this.platform.auth.token });
};

// ***************************************************
// *************** Transfer methods ******************
// ***************************************************
Locations.prototype.transfer = function(transfer){
    return this.platform.http.post('/transfers', transfer, { token: this.platform.auth.token });
};

Locations.prototype.listTransfers = function(query){
    return this.platform.http.get('/transfers', { query: query, token: this.platform.auth.token });
};

Locations.prototype.cancelTransfer = function(locationId){
    return this.platform.http.delete('/locations/' + locationId + '/transfer', { token: this.platform.auth.token });
};

Locations.prototype.acceptTransfer = function(transferId, body){
    return this.platform.http.put('/transfers/' + transferId + '/accept', body, { token: this.platform.auth.token });
};

Locations.prototype.resendTransfer = function(transferId){
    return this.platform.http.get('/transfers/' + transferId + '/resend', { token: this.platform.auth.token });
};

// ***************************************************
// **************** Invite methods *******************
// ***************************************************
Locations.prototype.invite = function(invite){
    return this.platform.http.post('/invite', invite, { token: this.platform.auth.token });
};

Locations.prototype.listInvitation = function(query){
    return this.platform.http.get('/invite', { query: query, token: this.platform.auth.token });
};

Locations.prototype.cancelInvitation = function(locationId, inviteId){
    return this.platform.http.delete('/locations/' + locationId + '/invite/' + inviteId, { token: this.platform.auth.token });
};

Locations.prototype.acceptInvite = function(inviteId, body){
    return this.platform.http.put('/invite/' + inviteId + '/accept', body, { token: this.platform.auth.token });
};

Locations.prototype.refuseInvite = function(inviteId){
    return this.platform.http.delete('/invite/' + inviteId, { token: this.platform.auth.token });
};

Locations.prototype.resendInvite = function(inviteId){
    return this.platform.http.get('/invite/' + inviteId + '/resend', { token: this.platform.auth.token });
};

// ***************************************************
// ************ Location Group methods ***************
// ***************************************************
Locations.prototype.getGroups = function(locationId, populate){
    var query = { populate: populate }
    return this.platform.http.get('/locations/' + locationId + '/groups', { query: query, token: this.platform.auth.token });
};

Locations.prototype.patchMember = function(locationId, payload){
    return this.platform.http.patch('/locations/' + locationId + '/groups/member', payload, { token: this.platform.auth.token });
};

Locations.prototype.removeMember = function(locationId, userId){
    return this.platform.http.delete('/locations/' + locationId + '/groups/member/' + userId, { token: this.platform.auth.token });
};

Locations.prototype.disableMember = function(locationId, payload){
    return this.platform.http.patch('/locations/' + locationId + '/groups/member/disable', payload, { token: this.platform.auth.token });
};

// ***************************************************
// *********** Location Support methods **************
// ***************************************************
Locations.prototype.getSupportState = function(locationId){
    return this.platform.http.get('/locations/' + locationId + '/support', { token: this.platform.auth.token });
};

Locations.prototype.enableSupport = function(locationId, payload){
    return this.platform.http.post('/locations/' + locationId + '/support', payload, { token: this.platform.auth.token });
};


module.exports = function(options) {
    options = options || {};
    return new Locations(options);
};

