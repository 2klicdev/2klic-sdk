var socketIOClient = require('socket.io-client');
var events = require('events');
var inherits = require('inherits');
var _ = require('lodash');

function Subscriber(channelString, handler) {
    this.channelString = channelString;
    this.handler = handler;
}

Subscriber.prototype.handle = function(event) {

    if(this.channelString.indexOf('*') !== -1) {
        var channel = this.channelString.split('/')[0];
        if(event.type.indexOf(channel) !== -1) {
            this.handler(event);
        }
    }
    else if(this.channelString === event.type) {
        return this.handler(event);
    }

};

function Channel(key, api) {
    this.key = key;
    this.api = api;
    events.EventEmitter.call(this);
}
inherits(Channel, events.EventEmitter);

/*
 * Subscribe to the channel using scope token and
 * @param: options
 *     token: A valid platform token providing access to private events as per the token scope
 *     type: A string or array of event type to be notified (optional)
 *     handler: A function that will be called with the event data
 *     scope: The scope that will be used when calling the callback. default to global.
 *     persistence: (optional). Provide a persistence strategy for unreceived events. (TBD)
 */
Channel.subscribe = function(options) {
    var subscribeKey = "/" + this.key + "/";
    if(_.isString(options.type)) {
        subscribeKey += options.type;
    }
    else {
        subscribeKey += "*";
    }

    return this.api.subscribe(subscribeKey, (function(options) {
        var types = [];
        if(_.isString(options.type)) {
            types = options.type.split(',');
        }
        else {
            types = options.types;
        }

        return function(event) {
            if(types.indexOf(event.type) !== -1) {
                options.handler.call(options.scope || this, event);
            }
        }
    })(options), { token: options.token, persistence: options.persistence });
};

function _handleEvent(event) {
    var _this = this;

    _.each(this.subscribers, function(subscriber) {

        // Provide a simple mechanism to handle replies to specific events
        subscriber.handle(event, function(reply) {
            _this.socket.emit('reply', {
                replyTo: event.id,
                data: reply
            });
        });

    });

}

function StreamAPI(options) {
    options = options || {};

    this.url = options.url;
    this.subscribers = [];

    this.socket = socketIOClient(options.url, options.socket);

    events.EventEmitter.call(this);

    // Connect all stream API event handlers
    this.socket.on('connect', function() {
        console.log("Stream API is CONNECTED to %s", this.url);
        this.emit('connect');
    }.bind(this));

    this.socket.on('disconnect', function() {
        console.log("Stream API is DISCONNECTED from %s", this.url);
        this.emit('disconnect');
    }.bind(this));

    this.socket.on('event', _handleEvent.bind(this));

    // Create all secure channels
    StreamAPI.catalog = new Channel("catalog", this);
    StreamAPI.klic = new Channel("2klic", this);
    StreamAPI.marketplace = new Channel("marketplace", this);
    StreamAPI.social = new Channel("social", this);
    StreamAPI.profile = new Channel("profile", this);
    StreamAPI.system = new Channel("system", this);

}

inherits(StreamAPI, events.EventEmitter);

StreamAPI.prototype.subscribe = function(channelString, handler, options) {
    options = options || {};

    // Register a listener in our socket
    this.subscribers.push(new Subscriber(channelString, handler));

    this.socket.emit('subscribe', {
        channel: channelString,
        token: options.token,
        persistence: options.persistence
    });
};

module.exports = StreamAPI;