var gulp = require('gulp');
var gutil = require('gulp-util');
var sourcemaps = require('gulp-sourcemaps');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var watchify = require('watchify');
var browserify = require('browserify');
var testem = require('gulp-testem');
var mocha = require('gulp-mocha');
//var jsdoc = require("gulp-jsdoc");

var bundler = watchify(browserify('./src/index.js', {
    standalone: '2Klic',
    cache: {},
    packageCache: {},
    fullPaths: true
}));

// add any other browserify options or transforms here
bundler.transform('brfs');

gulp.task('js', bundle); // so you can run `gulp js` to build the file
bundler.on('update', bundle); // on any dep update, runs the bundler
bundler.on('log', gutil.log); // output build logs to terminal

function bundle() {
    return bundler.bundle()
        // log errors if they happen
        .on('error', gutil.log.bind(gutil, 'Browserify Error'))
        .pipe(source('2klic.js'))
        // optional, remove if you dont want sourcemaps
        .pipe(buffer())
        .pipe(sourcemaps.init({loadMaps: true})) // loads map from browserify file
        .pipe(sourcemaps.write('./')) // writes .map file
        //
        .pipe(gulp.dest('./dist'));
}

gulp.task('test', function() {
    return gulp.src('test/**/*-spec.js', {read: false})
        .pipe(mocha({reporter: 'nyan'}));
});

/**
 * Test distribution file in a browser using testem
 */
gulp.task('e2e-test', function () {

    bundle();

    gulp.watch('./src/**/*.js', bundle);
    gulp.watch('./test/**/*.js', bundle);

    gulp.src([''])
        .pipe(testem({
            configFile: 'testem.json'
        }));
});

gulp.task('doc', function() {
    return gulp.src("./src/**/*.js")
        .pipe(jsdoc('./dist/apidoc'));
});