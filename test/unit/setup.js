// Export modules to global scope as necessary (only for testing)
var SDK = require('../../src');
var _ = require('lodash');

module.exports = {
    platformVersion: '0.0.0',
    platformName: "2klic-io",
    newInstance: function(options) {
        return new SDK(options);
    },
    credentials: require('../data/credentials.js')
};